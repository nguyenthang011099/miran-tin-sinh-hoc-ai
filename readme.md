# miRNA Project
Trong đề tài này, nhóm em đã thiết kế một phương pháp tiếp cận dựa trên DeepLearning để dự đoán chính xác các mục tiêu miRNA của con người. So sánh với các phương pháp học máy hiện đại khác và các công cụ dự đoán mục tiêu miRNA hiện có chỉ ra rằng mô hình của nhóm đã cải thiện hiệu suất dự đoán lên đáng kể.

##	Mô tả bài toán
Đầu vào cho các công cụ là trình tự của miRNA và gen được sắp xếp từ 5 ' đến> 3'. Công cụ của nhóm em sẽ sắp xếp lại trình tự miRNA tự động từ 5 ′ đến> 3 ′, 3 ′ đến> 5 ′ để phù hợp với yêu cầu của mô hình. Để hỗ trợ việc sử dụng, nhóm em đã thiết kế tự động chia các chuỗi gen dài thành các chuỗi ngắn chồng chéo có thể phù hợp với đầu vào của các mô hình khác nhau. Hơn nữa, một vài thông số có thể được điều chỉnh dựa trên mục tiêu của công việc của người dùng. Ví dụ, xác suất để xác định xem một trình tự ngắn có phải là vị trí liên kết của miRNA hay không và số lượng vị trí đích trong một gen để xác định liệu gen đó có phải là gen đích hay không. 

Kết quả đầu ra được lưu dưới dạng tệp fasta chứa các trình tự của các trang được nhắm mục tiêu.

## Tập dữ liệu sử dụng
Bộ dữ liệu được sử dụng chứa trình tự miRNA và gen của con người được nhóm em thu thập từ các nghiên cứu DeepMirTar và MiRAW (các nghiên cứu này sẽ được nêu đến ở phần III bên dưới) 
Tập dữ liệu đầu tiên được tải xuống từ Additional file tables trong bài nghiên cứu DeepMirTar, tập dữ liệu này chứa 3915 positive pairs của miRNA:target và 3905 negative pairs của miRNA:target. Tuy nhiên vì có các miRNA không có trong miRbase phiên bản mới nhất nên nhóm em đã xóa những miRNA này. Cuối cùng chỉ có 3908 positive pairs và 3898 negative pairs được giữ lại.
Tập dữ liệu thứ hai được nhóm em thu thập từ bài nghiên cứu miRAW. Trong nghiên cứu này, họ đã thu thập được một lượng lớn dữ liệu đã được xác minh bao gồm các miRNA:target pairs canonical và non-canonical. Tổng cộng có tất cả 32,726 positive và 31,992 negative pairs. Tuy nhiên cũng giống như tập dữ liệu đầu tiên thì nhóm đã loại bỏ những miRNA không có trong miRbase, vậy nên còn lại 31,660 positive và 30,993 negative pairs

## Chạy Project
predict_onemiRmultimRNA.py can be used to predict the targets for one miRNA. An example is shown here:

    python predict_onemiRmultimRNA.py -i1 data/tests/mrna_multiple_mixedLongShort.fa -i2 data/tests/mirna_hsa-miR-139-5p.txt -o results/tests/mir139-5p_predictedTar.fa -p 0.8 -ns 1 -s 22 
The meaning of the parameters can be obtained by python predict_onemiRmultimRNA.py -h

predict_multimiRmultimRNA.py can be used to predict the targets for multiple miRNAs. An example is shown here: 

    python predict_multimiRmultimRNA.py -i1 data/tests/mrna_multiple_mixedLongShort.fa -i2 data/tests/mirna_multiple.fa -o results/tests/mirna_multiple_predictedTar.fa -s 22 -p 0.8 -ns 1
The meaning of the parameters are the same as predict_onemiRmultimRNA.py and can be otained by python predict_multimiRmultimRNA.py -h

**The model used in the predict_multimiRmultimRNA.py and predict_onemiRmultimRNA.py is miTAR. If you would like to use a different model, please replace the model with another one, and change the fragment length to match the model input.**

## Yêu cầu

python: >=3.6

tensorflow: 1.14.0

keras: 2.2.4

h5py: 2.9.0

numpy

matplotlib

scipy

sklearn
